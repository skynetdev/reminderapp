<?php

namespace App\Http\Controllers;

use App\Classes\DatabaseHandler;
use App\Objects\CategoryObj;
use App\Objects\ReminderObj;
use Illuminate\Http\Request;

class ReminderController extends Controller
{
    public $databaseHandler;

    public function __construct()
    {
        $this->databaseHandler = new DatabaseHandler();
    }

    //Haal alle reminders op
    public function getReminders()
    {
        $reminders = $this->databaseHandler->getReminders();
        return $reminders;
    }

    public function getReminderByID($id)
    {
        $reminder = $this->databaseHandler->getReminderByID($id);
        return \Response::json($reminder);
    }

    public function addReminder(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $guessExtension = $image->getClientOriginalExtension();
            $fileName = time().'.'.$guessExtension;
            $destinationPath = storage_path() . '/app/uploads/images/reminder/' ;
            $image->move($destinationPath, $fileName);
            $request->image= $fileName;
        }
        $reminder = new ReminderObj($request);
        $data=$this->databaseHandler->addReminder($reminder);
        return \Response::json($data);
    }

    public function deleteReminderByID($id)
    {
        $deletedReminder = $this->databaseHandler->deleteReminderByID($id);
        return $deletedReminder;
    }

    public function editReminderByID($id, Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $guessExtension = $image->getClientOriginalExtension();
            $fileName = time().'.'.$guessExtension;
            $destinationPath = storage_path() . '/app/uploads/images/reminder/' ;
            $image->move($destinationPath, $fileName);
            $request->image= $fileName;
        }
        $reminder = new ReminderObj($request);
        $this->databaseHandler->editReminderByID($reminder, $id);
        return \Response::json($reminder);
    }

    public function getCategories()
    {
        $reminders = $this->databaseHandler->getCategories();
        return $reminders;
    }

    public function getCategoryID($id)
    {
        $reminder = $this->databaseHandler->getCategoryID($id);
        return \Response::json($reminder);
    }

    public function addCategory(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $guessExtension = $image->getClientOriginalExtension();
            $fileName = time().'.'.$guessExtension;
            $destinationPath = storage_path() . '/app/uploads/images/reminder/' ;
            $image->move($destinationPath, $fileName);
            $request->image= $fileName;
        }
        $reminder = new CategoryObj($request);
        $data=$this->databaseHandler->addCategory($reminder);
        return \Response::json($data);
    }

    public function deleteCategory($id)
    {
        $deletedReminder = $this->databaseHandler->deleteCategory($id);
        return $deletedReminder;
    }

    public function editCategory($id, Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $guessExtension = $image->getClientOriginalExtension();
            $fileName = time().'.'.$guessExtension;
            $destinationPath = storage_path() . '/app/uploads/images/reminder/' ;
            $image->move($destinationPath, $fileName);
            $request->image= $fileName;
        }
        $reminder = new CategoryObj($request);
        $this->databaseHandler->editCategory($reminder, $id);
        return \Response::json($reminder);
    }
}
