<?php

namespace App\Objects;
use Carbon\Carbon;

class CategoryObj
{
    public $id;
    public $name;

    function __construct($data)
    {
        $this->user_id= $data->user_id;
        $this->name = (!empty($data->name) ? $data->name : 'standard title');
    }
}
