<?php

namespace App\Objects;
use Carbon\Carbon;

class ReminderObj
{
    public $id;
    public $title;
    public $body;
    public $remainderDt;
    public $type;
    public $image;public $user_id;public $cat_id;

    function __construct($data)
    {
        $this->image= $data->image;
        $this->title = (!empty($data->title) ? $data->title : 'standard title');
        $this->body = (!empty($data->body) ? $data->body : 'standard body');
        $this->remainderDt = (!empty($data->remainderDt) ? Carbon::Parse($data->remainderDt) : Carbon::now()->addDay(7));
        $this->type = (!empty($data->type) ? $data->type : null);
        $this->user_id = (!empty($data->user_id) ? $data->user_id : null);
        $this->cat_id = (!empty($data->cat_id) ? $data->cat_id : null);

    }
}
