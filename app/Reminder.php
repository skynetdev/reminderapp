<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    //
    public function reminderType(){
        return $this->belongsTo(ReminderType::class,'repeat_type');
    }
}
