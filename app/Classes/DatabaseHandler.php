<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 16-12-2018
 * Time: 18:13
 */

namespace App\Classes;

use App\Category;
use App\Interfaces\DatabaseHandlerInterface;
use App\Objects\CategoryObj;
use App\Objects\ReminderObj;
use App\Reminder;
use Auth;

class DatabaseHandler implements DatabaseHandlerInterface
{

    public function getReminders()
    {

        //alle reminders ophalen
        $get = Reminder::all();
        $reminders = [];
        foreach ($get as $currentReminder) {
            $reminder = new ReminderObj($currentReminder);
            $reminder->id = $currentReminder->id;
            $reminders[] = $reminder;
        }
        return $reminders;
    }

    //reminder ophalen bij id
    public function getReminderByID($id)
    {
        $get = Reminder::find($id);
        $reminder = new ReminderObj($get);
        $reminder->id = $id;
        return $reminder;
    }

    //reminder toevoegen
    public function addReminder(ReminderObj $reminder)
    {
        $newReminder = new Reminder();
        $newReminder->title = $reminder->title;
        $newReminder->body = $reminder->body;
        $newReminder->reminder_dt = $reminder->remainderDt;
        $newReminder->repeat_type = $reminder->type;
        $newReminder->image = $reminder->image;
        $newReminder->save();
        return $newReminder;
    }

    //Reminder verwijderen
    public function deleteReminderByID($id)
    {
        $reminder = Reminder::find($id);
        $reminder->delete();

    }

    //Reminder bewerken
    public function editReminderByID(ReminderObj $reminder, $id)
    {
        $editReminder = Reminder::find($id);
        $editReminder->title = $reminder->title;
        $editReminder->body = $reminder->body;
        $editReminder->reminder_dt = $reminder->remainderDt;
        $editReminder->repeat_type = $reminder->type;
        $editReminder->image = $reminder->image;
        $editReminder->save();
    }
    //Category Section
    public function getCategories()
    {
        $get = Category::where(function ($qur){
            $qur->whereNull('user_id')->orWhere('user_id',Auth::user()->id);
        })->get();
        $categories = [];
        foreach ($get as $currentCategory) {
            $category = new CategoryObj($currentCategory);
            $category->id = $currentCategory->id;
            $categories[] = $category;
        }
        return $categories;
    }

    //reminder ophalen bij id
    public function getCategoryID($id)
    {
        $get = Category::find($id);
        $category = new CategoryObj($get);
        $category->id = $id;
        return $category;
    }

    public function addCategory(CategoryObj $category)
    {

        $newcategory = new Category();
        $newcategory->name = $category->name;
        $newcategory->user_id=Auth::user()->id;
        $newcategory->save();
        return $newcategory;
    }
    public function deleteCategory($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function editCategory(CategoryObj $category, $id)
    {
        $editCategory = Category::find($id);
        $editCategory->name = $category->title;
        $editCategory->user_id=Auth::user()->id;
        $editCategory->save();
    }
}
