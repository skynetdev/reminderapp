<?php


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::post('logout','API\UserController@logoutApi');
});
Route::any('getCategories', 'ReminderController@getCategories');
Route::get('getCategoryID/{id}', 'ReminderController@getCategoryID');
Route::post('addCategory', 'ReminderController@addCategory');
Route::post('editCategory/{id}', 'ReminderController@editCategory');
Route::delete('deleteCategory/{id}', 'ReminderController@deleteCategory');
Route::get('getReminders', 'ReminderController@getReminders');
Route::get('getReminderByID/{id}', 'ReminderController@getReminderByID');
Route::post('addReminder', 'ReminderController@addReminder');
Route::post('editReminderByID/{id}', 'ReminderController@editReminderByID');
Route::delete('deleteReminderByID/{id}', 'ReminderController@deleteReminderByID');
